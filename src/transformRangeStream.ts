import { Duplex } from "stream";
import { Vinyl, MultiVinylConverter } from "./vinyl.type";

export class TransformRangeStream extends Duplex implements NodeJS.ReadWriteStream {
  private files: Vinyl[] = [];
  private piped: NodeJS.WritableStream;

  constructor(private convertor: MultiVinylConverter) {
    super();
  }

  // need to implement this, otherwise throws
  public _read(size: number): void {
    this.piped = this.piped;
  }

  public write(file: any): boolean {
    this.files.push(file);
    return true;
  }

  public async end(): Promise<void> {
    if (this.piped == null) { return; }
    try {
      const converted = await this.convertor(this.files);
      converted.forEach(file => this.piped.write(file));
      this.piped.end();
    } catch (err) {
      this.piped.emit("error", err);
    }
  }

  public pipe<T extends NodeJS.WritableStream>(destination: T, options?: {
    end?: boolean;
  }): T {
    this.piped = destination;
    return super.pipe(destination, options);
  }
}

const typeRegex = /(?:\.[^.\\\/]*)?$/;

export function changeFileType(name: string, newType: string): string {
  if (newType.length > 0 && !newType.startsWith(".")) {
    newType = "." + newType;
  }

  return name.replace(typeRegex, newType);
}

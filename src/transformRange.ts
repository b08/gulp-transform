import { MultiFileConverter } from "./file.type";
import { MultiVinylConverter, Vinyl } from "./vinyl.type";
import { TransformRangeStream } from "./transformRangeStream";
import { readVinyl, toVinyl } from "./convertVinyl";

export function transformRange(converter: MultiFileConverter): NodeJS.ReadWriteStream {
  return new TransformRangeStream(convertFiles(converter));
}

function convertFiles(converter: MultiFileConverter): MultiVinylConverter {
  return async function (vinyls: Vinyl[]): Promise<Vinyl[]> {
    const files = await Promise.all(vinyls.map(readVinyl));
    const converted = await converter(files);
    return converted.map(toVinyl);
  };
}

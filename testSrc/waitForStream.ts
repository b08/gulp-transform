export function waitForStream(stream: NodeJS.ReadWriteStream): Promise<void> {
  return new Promise((resolve, reject) => {
    stream.on("error", reject);
    stream.on("end", resolve);
  });
}

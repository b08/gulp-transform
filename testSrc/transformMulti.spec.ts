import { test } from "@b08/test-runner";
import * as fse from "fs-extra";
import { dest, src } from "gulp";
import { transformRange } from "../src";
import { waitForStream } from "./waitForStream";

test("transformMulti", async expect => {
  const stream = src("./testFolder/sourceFile*.multi.txt")
    .pipe(transformRange(files => ([{
      ...files[0],
      name: "result.multi",
      contents: "multi results " + files.map(f => f.contents).join(" ")
    }])))
    .pipe(dest("./resultFolder"));
  await waitForStream(stream);
  const file = "./resultFolder/result.multi.txt";
  const result = await fse.readFile(file, "utf8");

  expect.equal(result, "multi results sourceFile1 multi sourceFile2 multi");
  await fse.remove(file);
});

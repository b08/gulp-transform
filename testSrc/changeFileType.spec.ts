import { changeFileType } from "../src";
import { describe } from "@b08/test-runner";

describe("changeFileType", it => {
  it("should remove type of file", expect => {
    // arrange
    const src = "file.type";

    // act
    const result = changeFileType(src, "");

    // assert
    expect.equal(result, "file");
  });

  it("should add type of file", expect => {
    // arrange
    const src = "file";

    // act
    const result = changeFileType(src, "type");

    // assert
    expect.equal(result, "file.type");
  });

  it("should change type of file", expect => {
    // arrange
    const src = "file.type";

    // act
    const result = changeFileType(src, ".state");

    // assert
    expect.equal(result, "file.state");
  });

  it("should change type of file", expect => {
    // arrange
    const src = "file.type";

    // act
    const result = changeFileType(src, ".state");

    // assert
    expect.equal(result, "file.state");
  });

  it("should change type of file with path", expect => {
    // arrange
    const src = "../file.type";

    // act
    const result = changeFileType(src, ".state");

    // assert
    expect.equal(result, "../file.state");
  });

  it("should add type to file with path", expect => {
    // arrange
    const src = "../file";

    // act
    const result = changeFileType(src, ".state");

    // assert
    expect.equal(result, "../file.state");
  });

  it("should remove type from file with path", expect => {
    // arrange
    const src = "../file.type";

    // act
    const result = changeFileType(src, "");

    // assert
    expect.equal(result, "../file");
  });
});

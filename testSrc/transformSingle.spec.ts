import { test } from "@b08/test-runner";
import { dest, src } from "gulp";
import { transform } from "../src";
import * as fse from "fs-extra";
import { waitForStream } from "./waitForStream";

test("transformSingle", async expect => {
  const stream = src("./testFolder/sourceFile*.single.txt")
    .pipe(transform(file => ({
      ...file,
      name: "result." + file.name,
      contents: "results for " + file.contents
    })))
    .pipe(dest("./resultFolder"));
  await waitForStream(stream);
  const file1 = "./resultFolder/result.sourceFile1.single.txt";
  const file2 = "./resultFolder/result.sourceFile2.single.txt";
  const [result1, result2] = await Promise.all([
    fse.readFile(file1, "utf8"),
    fse.readFile(file2, "utf8")
  ]);

  expect.equal(result1, "results for sourceFile1 single");
  expect.equal(result2, "results for sourceFile2 single");
  await Promise.all([fse.remove(file1), fse.remove(file2)]);
});

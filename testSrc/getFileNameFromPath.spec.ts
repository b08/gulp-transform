import { getFileNameFromPath, getExtensionFromPath } from "../src/getFileNameFromPath";
import { describe } from "@b08/test-runner";

describe("getFileNameFromPath", it => {
  it("should return file name without extension", expect => {
    // arrange
    const src = "c:\\folder.type\\file.type.ts";

    // act
    const result = getFileNameFromPath(src);

    // assert
    expect.equal(result, "file.type");
  });

});

describe("getExtensionFromPath", it => {
  it("should return file extension with dot", expect => {
    // arrange
    const src = "c:\\folder.type\\file.type.ts";

    // act
    const result = getExtensionFromPath(src);

    // assert
    expect.equal(result, ".ts");
  });
});
